CREATE TABLE IF NOT EXISTS country_state_city_mappings (
	seq_num SERIAL CONSTRAINT PK_country_state_city_mappings_seq_num PRIMARY KEY,
	country_id INT,
	country_name VARCHAR(20),
	state_id INT,
	state_name VARCHAR(20),
	city_id INT,
	city_name VARCHAR(20),
	created_date TIMESTAMP,
	updated_date TIMESTAMP
);