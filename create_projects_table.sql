CREATE TABLE IF NOT EXISTS projects (
	project_id SERIAL CONSTRAINT PK_projects_project_id PRIMARY KEY,
	title VARCHAR(50),
	description VARCHAR(150),
	created_date TIMESTAMP,
	updated_date TIMESTAMP,
	ended_date TIMESTAMP,
	project_category_id INT,
	CONSTRAINT FK_projects_project_category_id FOREIGN KEY 
	(project_category_id) REFERENCES category(category_id)
);