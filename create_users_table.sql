CREATE TABLE IF NOT EXISTS users (
	user_id SERIAL CONSTRAINT PK_users_user_id PRIMARY KEY,
	user_name VARCHAR(50),
	email VARCHAR(100),
	user_password VARCHAR(20),
	created_date TIMESTAMP,
	updated_date TIMESTAMP,
	country_id INT,
	state_id INT,
	city_id INT,
	user_type VARCHAR(20) CONSTRAINT CK_users_user_type 
	CHECK(user_type IN ('Normal', 'Admin', 'SuperAdmin'))
);